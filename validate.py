class ValidationModel(object):
    def __init__(self):
        self.model = {}

    def add_field(self, name, required=True, default=None, validator=None):
        self.model[name] = {'required': required, 'default': default, 'validator': validator}

    def test(self, data):
        issues = []
        for key in self.model:
            field = self.model[key]
            if field['required'] and (key not in data or len(str(data[key])) < 1):
                issues.append({'field': key, 'message': 'This field is required', 'value': ''})
            elif not field['required'] and (key not in data or len(str(data[key])) < 1):
                data[key] = field['default']
            elif field['validator'] is not None:
                message = field['validator'](data[key])
                if message is not None:
                    issues.append({'field': key, 'message': message, 'value': data[key]})
        if len(issues) > 0:
            raise Exception(issues)
        return data


class UserValidator(ValidationModel):
    def __init__(self, user, user_model):
        super(UserValidator, self).__init__()

        def valid_password(password):
            if len(password) < 8:
                return 'Passwords must be at least 8 characters long'

        def valid_email(email):
            if user['email'] != email and user_model.email_taken(email):
                return 'This email address is already in use by another user'
            if '@' not in email:
                return 'Not a valid email address'

        self.add_field('firstName')
        self.add_field('lastName')
        self.add_field('email', validator=valid_email)
        self.add_field('password', validator=valid_password)


class NewUserValidator(ValidationModel):
    def __init__(self, user_model):
        super(NewUserValidator, self).__init__()

        def valid_email(email):
            if user_model.email_taken(email):
                return 'This email address is already in use by another user'
            elif '@' not in email:
                return 'Not a valid email address'

        def valid_password(password):
            if len(password) < 8:
                return 'Passwords must be at least 8 characters long'

        self.add_field('firstName')
        self.add_field('lastName')
        self.add_field('email', validator=valid_email)
        self.add_field('password', validator=valid_password)


class GiftValidator(ValidationModel):
    def __init__(self):
        super(GiftValidator, self).__init__()

        def valid_link(link):
            if 'http://' not in link and 'https://' not in link:
                return 'Not a valid URL'

        self.add_field('title')
        self.add_field('description', required=False)
        self.add_field('price', required=False)
        self.add_field('image', required=False, validator=valid_link)
        self.add_field('link', required=False, validator=valid_link)