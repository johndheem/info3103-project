import MySQLdb
import MySQLdb.cursors
from flask import Flask, g, jsonify, make_response, request, render_template
from auth import requires_auth
from models import User, Gift
from resources import *
from validate import GiftValidator, UserValidator, NewUserValidator

app = Flask(__name__)


def connect_db():
    cursor = MySQLdb.cursors.DictCursor
    return MySQLdb.connect(host='localhost', user='root', passwd='', db='giftlist', cursorclass=cursor)


@app.before_request
def before_request():
    g.db = connect_db()


@app.teardown_request
def teardown_request(exception):
    db = getattr(g, 'db', None)
    if db is not None:
        db.close()


@app.route('/', methods=['GET'])
def static_homepage():
    return render_template('homepage.html')


@app.route('/register', methods=['GET'])
def static_register():
    return render_template('register.html')


@app.route('/gift', methods=['GET'])
def static_create_gift():
    return render_template('gift.html')


@app.route("/viewgift", methods=['GET'])
def static_open_gift():
    return render_template('giftItem.html')


@app.route('/profile', methods=['GET'])
def static_profile():
    return render_template('profile.html')


@app.route('/editprofile', methods=['GET'])
def static_editprofile():
    return render_template('editProfile.html')


@app.route('/people', methods=['GET'])
def static_people_list():
    return render_template('people.html')


@app.route('/api/users', methods=['GET'])
def get_user_list():
    model = User(g.db)
    return jsonify(user_list_resource(model.find_all()))


@app.route('/api/user', methods=['GET'])
@requires_auth
def get_current_user():
    return jsonify(user_resource(g.user))


@app.route('/api/user', methods=['POST'])
def register_user():
    model = User(g.db)
    validator = NewUserValidator(model)
    try:
        req = validator.test(request.json)
        user = model.create(req['firstName'], req['lastName'], req['email'], req['password'])
        return jsonify(user_resource(user))
    except Exception, e:
        return make_response(jsonify(error_resource(e)), 400)


@app.route('/api/user', methods=['PUT'])
@requires_auth
def update_user():
    model = User(g.db)
    validator = UserValidator(g.user, model)
    try:
        req = validator.test(request.json)
        user = model.update(g.user['id'], req['firstName'], req['lastName'], req['email'], req['password'])
        return jsonify(user_resource(user))
    except Exception, e:
        return make_response(jsonify(error_resource(e)), 400)


@app.route('/api/user/<int:user_id>', methods=['GET'])
def get_user(user_id):
    model = User(g.db)
    user = model.find(user_id)
    if user is None:
        return make_response('No such user', 404)
    else:
        return jsonify(user_resource(user))


@app.route('/api/user/<int:user_id>/gifts', methods=['GET'])
def get_user_gifts(user_id):
    gift_model = Gift(g.db)
    user_model = User(g.db)
    user = user_model.find(user_id)
    if user is None:
        return make_response("No such user", 404)
    else:
        gifts = gift_model.for_user(user_id)
        return jsonify(gift_list_resource(gifts))


@app.route('/api/gift', methods=['POST'])
@requires_auth
def create_gift():
    model = Gift(g.db)
    validator = GiftValidator()
    try:
        req = validator.test(request.json)
        gift = model.create(req['title'], req['description'], req['price'], req['image'], req['link'], g.user['id'])
        return jsonify(gift_resource(gift))
    except Exception, e:
        return make_response(jsonify(error_resource(e)), 400)


@app.route('/api/gift/<int:gift_id>', methods=['GET'])
def get_gift(gift_id):
    model = Gift(g.db)
    gift = model.find(gift_id)
    if gift is None:
        return make_response('No such gift', 404)
    else:
        return jsonify(gift_resource(gift))


@app.route('/api/gift/<int:gift_id>', methods=['PUT'])
@requires_auth
def update_gift(gift_id):
    model = Gift(g.db)
    gift = model.find(gift_id)
    validator = GiftValidator()
    if gift is None:
        return make_response('No such gift', 404)
    elif gift['user_id'] != g.user['id']:
        return make_response('Unauthorized', 401)
    else:
        try:
            req = validator.test(request.json)
            updated_gift = model.update(gift_id, req['title'], req['description'], req['price'], req['image'], req['link'])
            return jsonify(gift_resource(updated_gift))
        except Exception, e:
            return make_response(jsonify(error_resource(e)), 400)


@app.route('/api/gift/<int:gift_id>', methods=['DELETE'])
@requires_auth
def delete_gift(gift_id):
    model = Gift(g.db)
    gift = model.find(gift_id)
    if gift is None:
        return make_response("No such gift", 404)
    elif gift['user_id'] != g.user['id']:
        return make_response("Cannot delete another user's gift", 403)
    else:
        model.delete(gift_id)
        return make_response("Delete Successful", 200)


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


@app.route('/404', methods=['GET'])
def page_not_found_static():
    return render_template('404.html')


if __name__ == '__main__':
    app.run(host='localhost', port=3001, debug=True)
