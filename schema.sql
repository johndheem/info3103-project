drop table if exists users;
create table users (
  id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  firstName varchar(100) NOT NULL,
  lastName varchar(100) NOT NULL,
  email text NOT NULL,
  password text NOT NULL
);

drop table if exists gifts;
create table gifts (
  id integer primary key AUTO_INCREMENT,
  title text NOT NULL,
  description text,
  price DECIMAL(10, 2),
  image text,
  link text,
  user_id INT(11) NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users(id)
);