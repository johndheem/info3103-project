$(document).ready(function() {
    var loggedIn = localStorage.getItem('currentUser') != null;
    var userNavigation = loggedIn ? $('#nav-user') : $('#nav-guest');

    // Show the appropriate
    userNavigation.show();

    $('#logout').click(function(event) {
        localStorage.clear();
        window.location = '/';
        event.preventDefault();
    });
});