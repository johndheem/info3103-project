$(document).ready(function() {
    var loggedIn = localStorage.getItem('currentUser') != null;
    var user = loggedIn ? $.parseJSON(localStorage.getItem('currentUser')) : {};

    if(!loggedIn && !queryParams.id) {
        // Redirect to login if the user is requesting /profile without an ID and not logged in.
        window.location = '/';
    }

    var profileId = queryParams.id ? queryParams.id : user.id;
    var profile, gifts;
    var error = false;

    // Ran when both the profile and gift API calls respond.
    function onProfileLoaded(profile, gifts) {
        $('#page').show();

        // Show the first name.
        document.title = 'Gift List - ' + profile.firstName + ' ' + profile.lastName;
        $('#fullname').text(profile.firstName + ' ' + profile.lastName + '\'s Gift List');

        // Show the options if this is the current user's profile.
        if(user.id == profile.id) {
            $('#options').show();
        }

        // Show the gift list.
        if(gifts.length < 1) {
            $('#giftlist').append('<p style="text-align: center">No gifts added yet!</p>');
        }
        else {
            var row = 0;

            for (var i in gifts) {
                if(i % 3 == 0) {
                    row++;
                    $('#giftlist').append('<div id="giftlist-row-' + row + '" class="row"></div>');
                }

                var gift = gifts[i];
                var imageUrl = gift.image ? gift.image : '/static/img/default-gift.jpg';
                var price = gift.price ? '<p>$' + gift.price + '</p>' : '';
                var buyNowLink = gift.link ? '<a href="' + gift.link + '" class="btn btn-primary" role="button">Buy Now</a>' : '';

                var template = '<div class="col-sm-6 col-md-4"> \
                    <div class="thumbnail"> \
                        <img src="' + imageUrl + '" alt="Gift image"> \
                        <div class="caption"> \
                            <h3>' + gift.title + '</h3> \
                            ' + price + ' \
                            <p> \
                                <a href="/viewgift?id=' + gift.id + '" class="btn btn-default" role="button">More Info</a> \
                                ' + buyNowLink + ' \
                            </p> \
                        </div> \
                    </div> \
                </div>';

                $('#giftlist-row-' + row).append(template);
            }
        }
    }

    // If there is an error, load the 404 page into the content so it looks like a regular 404.
    function onProfileError() {
        $('#page').load('/404', function() {
            $(this).show();
        });
    }

    // Get the profile information.
    $.ajax({
        'url': '/api/user/' + profileId,
        'type': 'GET',
        'success': function(response) {
            profile = response;

            if(gifts) {
                onProfileLoaded(profile, gifts);
            }
        },
        'error': function(response) {
            if(!error) {
                error = true;
                onProfileError();
            }
        }
    });

    // Get the list of gifts for the user's profile.
    $.ajax({
        'url': '/api/user/' + profileId + '/gifts',
        'type': 'GET',
        'success': function(response) {
            gifts = response.gifts;

            if(profile) {
                onProfileLoaded(profile, gifts);
            }
        },
        'error': function(response) {
            if(!error) {
                error = true;
                onProfileError();
            }
        }
    });
});