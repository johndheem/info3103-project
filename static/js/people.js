$(document).ready(function() {
    $.ajax({
        'url': '/api/users',
        'type': 'GET',
        'success': function(response) {
            var users = response.users;
            console.log(users);

            for(var i in users) {
                var user = users[i];

                var template = '<div class="media"> \
                    <div class="media-left"> \
                        <img class="img-circle" src="' + user.image + '" alt="user image" style="max-width:50px;max-height:50px"> \
                    </div> \
                    <div class="media-body media-middle"> \
                        <h4 class="media-heading">\
                            <a href="/profile?id=' + user.id + '">' + user.firstName + ' ' + user.lastName + '</a>\
                        </h4> \
                    </div> \
                </div>';

                $('#people').append(template);
            }

            $('#page').show();
        },
        'error': function(response) {
            // There was an error, show the 404 page (nothing better to do)
            $('#page').load('/404', function() {
                $(this).show();
            })
        }
    })
});