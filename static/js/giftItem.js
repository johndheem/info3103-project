function deleteGift(gift, user) {
    return function(event) {
        event.preventDefault();

        if(confirm("Are you sure you want to delete this gift?")) {
            $.ajax({
                'url': '/api/gift/' + gift.id,
                'type': 'DELETE',
                'headers': {
                    'Authorization': 'Basic ' + btoa(user.email + ':' + user.password)
                },
                'success': function(response) {
                    window.location = '/profile';
                }
            })
        }
    }
}

$(document).ready(function() {
    var user = localStorage.getItem('currentUser') != null
        ? $.parseJSON(localStorage.getItem('currentUser'))
        : {};

    var giftId = queryParams.id;

    $.ajax({
        'url': '/api/gift/' + giftId,
        'type': 'GET',
        'success': function(response) {
            var gift = response;
            $('#page').show();
            var imageUrl = gift.image ? gift.image : '/static/img/default-gift.jpg';
            // image on the right
            // Description on the left
            var description = '<h2><small>Description: </small></h2>   <p> ' + gift.description + '</p>';
            var price = '<h2><small>Price: $ ' + gift.price + '</small></h2>';
            var buyNowLink = gift.link ? '<a href="' + gift.link + '" class="btn btn-primary" role="button">Buy Now</a>' : '';
            var optionButtons = '<a id="delete-gift" href="#" class="btn btn-danger">Delete</a>';

            if(gift.price === null){
                price = ' ';
            }

            if(gift.description === null){
                description = ' ';
            }

            if(gift.user_id != user.id) {
                optionButtons = '';
            }

            var template ='<div class="col-sm-6 col-md-4"> \
                <div class="container">	\
                    <div class="col-md-6"> \
                        <img src=\"' + imageUrl + ' "  class="img-rounded img-responsive" alt="Gift image"> \
                    </div> \
                    <div class="col-md-6"> \
                        <h1>'+ gift.title + '</h1> \
                        ' + description + ' \
                        ' + price + ' \
                        ' + buyNowLink + ' \
                        ' + optionButtons + '\
                    </div> \
                </div> \
            </div>';

            var giftItem = $('#giftItem');

            giftItem.append(template);
            giftItem.on('click', '#delete-gift', deleteGift(gift, user));
        },
        'error': function(response) {
            $('#page').load('/404', function() {
                $(this).show();
            });
        }
    });
});
