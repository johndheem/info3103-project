$(document).ready(function() {
    var loggedIn = localStorage.getItem('currentUser') != null;

    if(loggedIn) {
        window.location = '/profile';
    }

    // Handle the homepage form submission.
    $('#Login').submit(function(event) {
        // Remove any previous errors
        $('#login-error').hide();

        // Get the form data
        var email = $('#email').val();
        var password = $('#password').val();
        var currentUser = {"email":email, "password":password};

        $.ajax({
            'url': '/api/user',
            'contentType': 'application/json',
            headers: {'Authorization': "Basic " + btoa(email+ ":" + password)},
            'method': 'GET',
            success: function(response) {
                // If the get was successful, redirect to the profile screen.
                response.password = currentUser.password;
                localStorage.setItem('currentUser', JSON.stringify(response));
                window.location = '/profile';
            },
            error: function(response) {
                $('#login-error').show();
            }
        });

        event.preventDefault();
    });
});