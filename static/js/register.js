$(document).ready(function() {
    var loggedIn = localStorage.getItem('currentUser') != null;

    if(loggedIn) {
        window.location = '/profile';
    }

    // Handle the register form submission.
    $('#register').submit(function(event) {
        // Remove any previous errors
        $('.help-block').remove();
        $('.has-error').removeClass('has-error');

        // Get the form data
        var firstName = $('#firstName').val();
        var lastName = $('#lastName').val();
        var email = $('#email').val();
        var password = $('#password').val();

        $.ajax({
            'url': '/api/user',
            'contentType': 'application/json',
            'method': 'POST',
            'data': JSON.stringify({
                'firstName': firstName,
                'lastName': lastName,
                'email': email,
                'password': password
            }),
            success: function(response) {
                // If the post was successful, redirect to the login screen.
                window.location = '/';
            },
            error: function(response) {
                var errors = response.responseJSON.errors;
                console.log(errors);

                for(var i in errors) {
                    var group = $('#' + errors[i].field).closest('.form-group');

                    group.append('<span class="help-block">' + errors[i].message + '</span>');
                    group.addClass('has-error');
                }
            }
        });

        event.preventDefault();
    });
});