$(document).ready(function() {
    var loggedIn = localStorage.getItem('currentUser') != null;

    if(!loggedIn) {
        // If the user is not logged in, redirect to the home page.
        window.location = '/';
    }

    var user = $.parseJSON(localStorage.getItem('currentUser'));

    $('#firstName').val(user.firstName);
    $('#lastName').val(user.lastName);
    $('#email').val(user.email);

    // Handle the edit profile form submission.
    $('#editprofile').submit(function(event) {
        // Remove any previous errors
        $('.help-block').remove();
        $('.has-error').removeClass('has-error');

        // Get the form data
        var firstName = $('#firstName').val();
        var lastName = $('#lastName').val();
        var email = $('#email').val();
        var password = $('#password').val();
        var confirmPassword = $('#confirmPassword');

        if(password != confirmPassword.val()) {
            var group = confirmPassword.closest('.form-group');
            group.append('<span class="help-block">The passwords do not match</span>');
            group.addClass('has-error');
        }
        else {
            $.ajax({
                'url': '/api/user',
                'contentType': 'application/json',
                 headers: {'Authorization': "Basic " + btoa(user.email + ":" + user.password)},
                'method': 'PUT',
                'data': JSON.stringify({
                    'firstName': firstName,
                    'lastName': lastName,
                    'email': email,
                    'password': password
                }),
                success: function (response) {
                    // If the post was successful, redirect to the profile screen.
                    window.location = '/profile';
                },
                error: function (response) {
                    var errors = response.responseJSON.errors;
                    console.log(errors);

                    for (var i in errors) {
                        var group = $('#' + errors[i].field).closest('.form-group');

                        group.append('<span class="help-block">' + errors[i].message + '</span>');
                        group.addClass('has-error');
                    }
                }
            });
        }

        event.preventDefault();
    });
});