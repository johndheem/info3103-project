$(document).ready(function() {
    var loggedIn = localStorage.getItem('currentUser') != null;

    if(!loggedIn) {
        // User isn't logged in, so redirect to the homepage (login).
        window.location = '/';
    }

    // Handle the register form submission.
    $('#gift').submit(function(event) {
        // Remove any previous errors
        $('.help-block').remove();
        $('.has-error').removeClass('has-error');

        // Get the form data
        var title = $('#title').val();
        var description = $('#description').val();
        var price = $('#price').val();
        var image = $('#image').val();
        var link = $('#link').val();
        var user = $.parseJSON(localStorage.getItem('currentUser'));


        $.ajax({
            'url': '/api/gift',
            'contentType': 'application/json',
            headers: {'Authorization': "Basic " + btoa(user.email + ":" + user.password)},
            'method': 'POST',
            'data': JSON.stringify({
                'title': title,
                'description': description,
                'price': price,
                'image': image,
                'link': link
            }),
            success: function(response) {
                // If the post was successful, redirect to the profile screen.
                window.location = '/profile';
            },
            error: function(response) {
                var errors = response.responseJSON.errors;
                console.log(errors);

                for(var i in errors) {
                    var group = $('#' + errors[i].field).closest('.form-group');

                    group.append('<span class="help-block">' + errors[i].message + '</span>');
                    group.addClass('has-error');
                }
            }
        });

        event.preventDefault();
    });
});