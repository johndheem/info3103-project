import urllib, hashlib


def get_user_image(user):
    size = 100
    default = 'https://tracker.moodle.org/secure/attachment/30912/f3.png'
    url = 'http://www.gravatar.com/avatar/' + hashlib.md5(user['email'].lower()).hexdigest() + '?'
    url += urllib.urlencode({'d': default, 's': size})
    return url


def user_resource(user):
    resource = dict(user)
    resource['image'] = get_user_image(user)
    del resource['password']
    return resource


def user_list_resource(users):
    resources = []
    for user in list(users):
        resources.append(user_resource(user))
    return {'users': resources}


def gift_resource(gift):
    resource = dict(gift)
    if resource['price'] is not None:
        resource['price'] = str(resource['price'])
    return resource


def gift_list_resource(gifts):
    resources = []
    for gift in list(gifts):
        resources.append(gift_resource(gift))
    return {'gifts': resources}


def error_resource(exception):
    return {'errors': exception.message}