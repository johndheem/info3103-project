from functools import wraps
from flask import request, make_response, g
from models import User


def check_auth(username, password):
    model = User(g.db)
    user = model.check_login(username, password)
    g.user = user
    return user is not None


def authentication_error():
    return make_response('Invalid login credentials', 401)


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth or not check_auth(auth.username, auth.password):
            return authentication_error()
        return f(*args, **kwargs)
    return decorated
