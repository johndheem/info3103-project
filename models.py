from contextlib import closing


class User:
    def __init__(self, db):
        self.db = db

    def find(self, user_id):
        with closing(self.db.cursor()) as cursor:
            cursor.execute('SELECT * FROM users WHERE id = %s', [user_id])
            return cursor.fetchone()

    def find_all(self):
        with closing(self.db.cursor()) as cursor:
            cursor.execute('SELECT * FROM users ORDER BY firstName, lastName ASC')
            return cursor.fetchall()

    def check_login(self, email, password):
        with closing(self.db.cursor()) as cursor:
            cursor.execute('SELECT * FROM users WHERE email = %s AND password = %s', [email, password])
            return cursor.fetchone()

    def update(self, user_id, first, last, email, password):
        with closing(self.db.cursor()) as cursor:
            args = [first, last, email, password, user_id]
            cursor.execute('UPDATE users SET firstName = %s, lastName = %s, email = %s, password = %s WHERE id = %s', args)
            self.db.commit()
        return self.find(user_id)

    def create(self, first_name, last_name, email, password):
        with closing(self.db.cursor()) as cursor:
            cursor.execute('INSERT INTO users (firstName, lastName, email, password) VALUES (%s, %s, %s, %s)', [first_name, last_name, email, password])
            self.db.commit()
            return self.find(cursor.lastrowid)

    def email_taken(self, email):
        with closing(self.db.cursor()) as cursor:
            cursor.execute('SELECT COUNT(*) as taken FROM users WHERE email = %s', [email])
            result = cursor.fetchone()
            return int(result['taken']) > 0


class Gift:
    def __init__(self, db):
        self.db = db

    def update(self, gift_id, title, description, price, image, link):
        with closing(self.db.cursor()) as cursor:
            args = [title, description, float(price), image, link, gift_id]
            cursor.execute('UPDATE gifts SET title = %s, description = %s, price = %s, image = %s, link = %s WHERE id = %s', args)
            self.db.commit()
        return self.find(gift_id)

    def find(self, gift_id):
        with closing(self.db.cursor()) as cursor:
            cursor.execute('SELECT * FROM gifts WHERE id = %s', [gift_id])
            return cursor.fetchone()

    def delete(self,gift_id):
        with closing(self.db.cursor()) as cursor:
            cursor.execute('DELETE FROM gifts WHERE id = %s', [gift_id])
            self.db.commit()

    def create(self, title, description, price, image, link, user_id):
        with closing(self.db.cursor()) as cursor:
            args = [title, description, price, image, link, user_id]
            cursor.execute('INSERT INTO gifts (title, description, price, image, link, user_id) VALUES (%s, %s, %s, %s, %s, %s)', args)
            self.db.commit()
            return self.find(cursor.lastrowid)

    def for_user(self, user_id):
        with closing(self.db.cursor()) as cursor:
            cursor.execute('SELECT * FROM gifts WHERE user_id = %s ORDER BY id DESC', [user_id])
            return cursor.fetchall()
